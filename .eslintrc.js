module.exports = {
    "globals": {
        // GSAP / Tween
        "TimelineLite" : false,
        "TimelineMax" : false,
        "TweenLite" : false,
        "TweenMax" : false,
        "Back" : false,
        "Bounce" : false,
        "Circ" : false,
        "Cubic" : false,
        "Ease" : false,
        "EaseLookup" : false,
        "Elastic" : false,
        "Expo" : false,
        "Linear" : false,
        "Power0" : false,
        "Power1" : false,
        "Power2" : false,
        "Power3" : false,
        "Power4" : false,
        "Quad" : false,
        "Quart" : false,
        "Quint" : false,
        "RoughEase" : false,
        "Sine" : false,
        "SlowMo" : false,
        "SteppedEase" : false,
        "Strong" : false,
        "Draggable" : false,
        "SplitText" : false,
        "VelocityTracker" : false,
        "CSSPlugin" : false,
        "ThrowPropsPlugin" : false,
        "BezierPlugin" : false,

        // dependencies
        "React" : false,
        "ReactDOM" : false,
        "$" : false,
        "moment" : false,
        "JQuery" : false,
        "Tween" : false,
        "_" : false,
        "uuid" : false,

        // Promise
        "Promise" : false,

        // Typekit
        "Typekit" : false
    },
	"env": {
		"browser": true,
		"commonjs": true,
		"es6": true,
		"node": true,
	},
	// "extends": "airbnb",
	"extends": ["eslint:recommended", "plugin:react/recommended"],
    "parser": "babel-eslint",
	"parserOptions": {
		"sourceType": "module",
        "allowImportExportEverywhere": true,

        // Enable JSX
        "ecmaFeatures": {
            "jsx": true,
        },
	},
    // Enable eslint-plugin-react
    "plugins": [
        "react",
    ],
	"rules": {
		"comma-dangle": ["error", "always-multiline"],
		"indent": ["error", 4],
		"linebreak-style": ["error", "windows"],
		"quotes": ["error", "single"],
		"semi": ["error", "always"],
		"no-unused-vars": ["warn"],
		"no-console": 0,
	},
};
