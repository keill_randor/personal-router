import AppDispatcher from '../dispatcher/AppDispatcher';
import { actions as ActionTypes } from '../constants/RoutingConstants';

export default {

    updateUrl:(route, saveHistory) => {
        AppDispatcher.dispatch({
            actionType: ActionTypes.UPDATE_URL,
            data: route
        });
    },

    back:() => {
        AppDispatcher.dispatch({
            actionType: ActionTypes.BACK
        });
    }
}
