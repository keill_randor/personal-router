import RoutingActions from '../../actions/RoutingActions';

import RoutingStore from '../../stores/RoutingStore';

import Routes from './Routes.react';

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            route : true
        };

        this.onRoutingStoreChange = this.onRoutingStoreChange.bind(this);
    }

    componentDidMount() {
        RoutingStore.addChangeListener(this.onRoutingStoreChange);

        $(document).on('click', 'a', (e) => {
            e.preventDefault();
            RoutingActions.updateUrl({ url : e.target.href.replace(location.origin+'/', ''), hash : false });
        });

        window.onpopstate = function (e) {
            e.preventDefault();
            RoutingActions.back();
        };
    }

    onRoutingStoreChange(data) {
        // EXECUTE CODE HERE ON ROUTING ChANGES
    }

    render() {
        console.log('APP STATE', this.state);

        return (
            <div
                id='app'>

                <div style={{
                    'position' : 'absolute',
                    'top' : 'calc(10% - 100px)',
                    'left' : 'calc(50% - 110px',
                    'width' : '220px',
                    'height' : '200px'
                }}>


                    MAIN COMPONENT <br/><br/>

                    <a href='/member/'>Route :: member</a>
                    <br/>
                    <a href='/todo/'>Route :: todo</a>
                    <br/>
                    <a href='/member/67/'>Route :: member/67</a>
                    <br/>
                    <a href='/member/67/info/'>Route :: member/67/info</a>
                    <br/>
                    <a href='/todo/today/'>Route :: todo/today</a>
                </div>

                { this.state.route &&
                    <Routes />
                }
            </div>
        );
    }
}

export default App;
