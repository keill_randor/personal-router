import Deeper from '../deeper/Deeper.react';

class Routes extends React.Component {

    render() {


        return (


                <div style={{
                    'position' : 'absolute',
                    'top' : 'calc(25% - 100px)',
                    'left' : 'calc(50% - 110px',
                    'width' : '220px',
                    'height' : '200px'
                }}>

                    IN COMPONENT ROUTES

                    <br/><br/>

                    <a href='/route/member/'>Route :: member</a>
                    <br/>
                    <a href='/route/todo/'>Route :: todo</a>
                    <br/>
                    <a href='/route/member/67/'>Route :: member/67</a>
                    <br/>
                    <a href='/route/member/67/info/'>Route :: member/67/info</a>
                    <br/>
                    <a href='/route/todo/today/'>Route :: todo/today</a>

                    <Deeper />
                </div>

        );
    }
}

export default Routes;
