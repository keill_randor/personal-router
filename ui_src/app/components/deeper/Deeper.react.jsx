class Deeper extends React.Component {

    render() {
        return (


                <div style={{
                    'position' : 'absolute',
                    'top' : 'calc(140% - 100px)',
                    'left' : 'calc(50% - 110px',
                    'width' : '220px',
                    'height' : '200px'
                }}>

                    IN COMPONENT DEEPER ROUTES

                    <br/><br/>

                    <a href='/deeper'>Route :: deeper</a>
                    <br/>
                    <a href='/deeper/todo'>Route :: deeper/todo</a>
                    <br/>
                    <a href='/deeper/67/'>Route :: member/67</a>
                    <br/>
                    <a href='/deeper/67/info'>Route :: member/67/info</a>
                    <br/>
                    <a href='/deeper/today'>Route :: todo/today</a>
                </div>

        );
    }
}

export default Deeper;
