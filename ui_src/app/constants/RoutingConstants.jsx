// import keyMirror from 'keymirror';
var keyMirror = require('keyMirror');

module.exports = {

    endPoints: {
    },

    actions: keyMirror({
        UPDATE_URL              : null,
        BACK                    : null
    }),

    events: keyMirror({
        CHANGE_EVENT            : 'changeEvent'
    })
};
