// import app from './components/app/app';
// // import 'react';

// let demoComponent = app();

// document.body.appendChild(demoComponent);

// // HMR interface
// if (module.hot) {
//     // Capture hot update
//     module.hot.accept('./components/app/app', () => {
//         const nextComponent = app();
//         // Replace old content with the hot loaded one
//         document.body.replaceChild(nextComponent, demoComponent);
//         demoComponent = nextComponent;
//     });
// }

// import ReactDOM from 'react-dom';

// ReactDOM.render(
//     <div>Hello world!!213!</div>,
//     document.getElementById('template')
// );

if(process.env.NODE_ENV !== 'production') {
    console.log('IN DEV MODE');
    React.Perf = require('react-addons-perf');
}

// import React from 'react';
// import ReactDOM from 'react-dom';

// import Component from './component';
import App from './components/app/App.react';

import { AppContainer } from 'react-hot-loader';

const render = App => {
    ReactDOM.render(
        <AppContainer>
            <App />
        </AppContainer>,

        document.getElementById('template')
    );
};

render(App);

if (module.hot) {
    module.hot.accept('./components/app/App.react', () => render(App));
}
