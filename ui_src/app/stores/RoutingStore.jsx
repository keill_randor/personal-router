import AppDispatcher from '../dispatcher/AppDispatcher';
import { EventEmitter } from 'events';

import { actions as RoutingActions } from '../constants/RoutingConstants'
import { events as RoutingEvents } from '../constants/RoutingConstants'

let _state = {
    boot            : false,
    history         : []
};

class RoutingStoreClass extends EventEmitter {

    boot( data ) {
        _state.boot = true;

        var url = window.location.pathname;

        var route = {
            url : url.length > 1 ? url.substr(1) : 'dashboard',
            hash : window.location.hash.replace('#', ''),
            parts : this.spliceUrl(url)
        };

        // Removes the `#`, and any leading/final `/` characters
        // window.location.hash.replace(/^#\/?|\/$/g, '').split('/');

        _state.history.push(route);

        this.emitChange(route);
    }

    updateRoute(route) {

        console.log('href', window.location.href);
        console.log('hostname', window.location.hostname);
        console.log('pathname', window.location.pathname);

        route.parts = this.spliceUrl(route.url);

        if (_state.history.length > 0 && _state.history[_state.history.length - 1].url === route.url)
            return;

        var pageObj = {
            title: 'Suz Organizer',
            urlHash: route.url
        };

        window.history.pushState( null , null , '/'+route.url);
        _state.history.push(route);

        this.emitChange(route);
    }

    spliceUrl(url) {
        var parts = url.split('/');

        for (var i=parts.length - 1; i>=0; i--) {
            if (parts[i] === '')
                parts.splice(i, 1);
            else
                parts[i] = parts[i].toLowerCase();
        }

        return parts;
    }

    historyBack() {
        if (_state.history.length > 1)
            _state.history.splice(-1, 1);

        var route = _state.history[_state.history.length - 1];

        var pageObj = {
            title: 'Suz Organizer',
            urlHash: route.url
        };

        window.history.pushState( null , null , '/'+route.url);

        this.emitChange(route);
    }

    emitChange( callback ) {
        this.emit(RoutingEvents.CHANGE_EVENT, callback);
    }

    addChangeListener( callback ) {
        this.on(RoutingEvents.CHANGE_EVENT, callback)
    }

    removeChangeListener( callback ) {
        this.removeListener(RoutingEvents.CHANGE_EVENT, callback)
    }
}

const RoutingStore = new RoutingStoreClass();

RoutingStore.dispatchToken = AppDispatcher.register(action => {

    switch(action.actionType) {

        case RoutingActions.UPDATE_URL:
            RoutingStore.updateRoute(action.data)
            break;

        case RoutingActions.BACK:
            RoutingStore.historyBack(action.data)
            break;

        default:
    }

});

export default RoutingStore;

/*



If your application is not deployed at the root (such as with a virtual directory, or just in a deeper hierarchy) then you'll end up screwing up the URL if you do history.pushState({}, '/newpath');

There's a couple alternatives :

1) Get your root path in a javascript variable that you can just prepend the URL to it. In this example window.ViewModel.rootPath is just an arbitrary place - just modify this for however you want to store global variables.

You will have to set the rootPath in script (example here for .NET).

<script>
    window.ViewModel.rootPath = "@Request.ApplicationPath";
</script>

history.pushState({}, window.ViewModel.rootPath + '/newpath');

2) Check if the URL ends with a / and if it does you need to go up 1 directory level. Note: This approach requires you to know the 'parent' directory of what you're posting - in this case YourAccount.

history.pushState({ mode: 'Club' }, '',
     (window.location.href.endsWith('/') ? '../' : '') +
      'YourAccount/ManageClub/' + id );

If the current URL is /preview/store/YourAccount then this will become ../YourAccount/ManageClub/123.

If the current URL is /preview/store/YourAccount/ then this will become YourAccount/ManageClub/123.

These with both end up at the same URL.


*/
