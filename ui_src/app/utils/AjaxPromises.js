/* STORES */
// import ConfigStore from '../stores/ConfigStore'

/* REQUESET */
import request from 'superagent/lib/client';

//var Promise;

export default {

    GET: ( url ) => {

        var promise = new Promise((resolve, reject) => {
            request
                .get('http://localhost/suz-organizer/api/' + url)
                .end((err, response) => {
                    if (err) reject(err);
                    resolve(JSON.parse(response.text));
                });
        });

        return promise;
    },

    POST: ( url, data ) => {

        var promise = new Promise((resolve, reject) => {

            $.ajax({
                url: 'http://localhost/suz-organizer/api/' + url,
                data: data,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data) {

                    if (!data.hasOwnProperty('message'))
                        data = JSON.parse(data);

                    if (data.message === 'error') {
                        resolve(false);
                    }
                    else
                        resolve(data);
                },
                error: function() {
                    reject(this);
                    console.log('error in post');
                },
            });
        });

        return promise;
    },
};
