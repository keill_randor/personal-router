const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const merge = require('webpack-merge');
const glob = require('glob');
const StyleLintPlugin = require('stylelint-webpack-plugin');
// const StyleLintSCSS = require('stylelint-scss');

const parts = require('./webpack.parts');

const PATHS = {
    ui_src: path.join(__dirname, 'ui_src'),
	app: path.join(__dirname, 'ui_src/app'),
	build: path.join(__dirname, 'build'),
    style: glob.sync('./ui_src/assets/css/main.scss'),
    assets: path.join(__dirname, 'ui_src/assets')
};



const common = merge([
    {
    	// Entry accepts a path or an object of entries.
    	// We'll be using the latter form given it's
    	// convenient with more complex configurations.
    	//
    	// Entries have to resolve to files! It relies on Node.js
    	// convention by default so if a directory contains *index.js*,
    	// it will resolve to that.
    	entry: {
    		// hmr: [
    		// 	// Include the client code.
    		// 	// Note how the host/port setting maps here.
    		// 	'webpack-dev-server/client?http://localhost:8080',
    		// 	// Hot reload only when compiled successfully
    		// 	'webpack/hot/only-dev-server',
    		// 	// Alternative with refresh on failure
    		// 	// 'webpack/hot/dev-server',
    		// ],
    		app: PATHS.app,
            style: PATHS.style,
    	},
    	output: {
            publicPath: '/',
    		path: PATHS.build,
    		filename: '[name].[chunkhash].js',
    	},
        resolve: {
            extensions: ['.js', '.jsx'],
        },
    	plugins: [
    		new HtmlWebpackPlugin({
    			title: 'Webpack demo',
    		}),

            // https://stylelint.io/user-guide/node-api/#options
            // using .stylelintrc for rules
            new StyleLintPlugin(),

            // https://www.npmjs.com/package/stylelint-scss
            // using .stylelintrc.json for rules
            // new StyleLintSCSS(),
    	],
    },
    parts.indexTemplate({
        title: 'Your Title',
        appMountId: 'template',
        baseHref: '',
        devServer: ''
    }),
    parts.loadJavaScript({ include: PATHS.app }),
    parts.loadFonts(),
    parts.loadImages({
        options: {
            limit: 15000,
            include: PATHS.assets
        },
    }),
    // parts.loadCSS(),

    parts.globalInclude(),
]);

module.exports = function(env) {

    console.log('Webpack --env ', env);

    process.env.BABEL_ENV = env;

    if (env === 'production') {
        return merge([
            common,
            {
                performance: {
                    hints: 'warning', // 'error' or false are valid too
                    maxEntrypointSize: 100000, // in bytes
                    maxAssetSize: 50000, // in bytes
                },
            },
            parts.setFreeVariable(
                'process.env.NODE_ENV',
                'production'
            ),
            parts.clean(PATHS.build),
            parts.minifyJavaScript({ useSourceMap: true }),
            parts.extractBundles({
                bundles: [
                    {
                        name: 'vendor',
                        entries: ['react', 'react-dom'],
                    },
                    {
                        name: 'manifest',
                    }
                ],
            }),
            parts.generateSourceMaps({ type: 'source-map' }),
            parts.lintJavaScript({ include: PATHS.app }),
            parts.purifyCSS({
                paths: glob.sync(path.join(PATHS.app, '*')),
            }),
            parts.extractCSS({ use: ['css-loader', 'sass-loader'] }),
        ]);
	}

    return merge([
        common,
        parts.dontParse({
            name: 'react',
            path: path.join(
                __dirname, 'node_modules', 'react'
            )
        }),
        {
            entry: {
                // react-hot-loader has to run before app!
                app: ['react-hot-loader/patch', PATHS.app],
            },
            output: {
                devtoolModuleFilenameTemplate: 'webpack:///[absolute-resource-path]',
                filename: '[name].js', // removed extra .
            },
            plugins: [
                new webpack.WatchIgnorePlugin([
                    path.join(__dirname, 'node_modules')
                ]),
            ],
        },
        parts.generateSourceMaps({ type: 'cheap-module-eval-source-map'}),
        parts.devServer({
            // Customize host/port here if needed
            host: process.env.HOST,
            port: 8000,
            path: PATHS.app
        }),
        parts.lintJavaScript({
            include: PATHS.app,
            options: {
                // Emit warnings over errors to avoid crashing
                // HMR on error.
                emitWarning: true,
            },
        }),
        parts.loadCSS(),
    ]);
};
